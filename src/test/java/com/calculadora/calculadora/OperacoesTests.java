package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperacaoDeSoma(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros), 6);
    }

    @Test
    public void testarOperacaoDeSomaDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,1), 2);
    }

    @Test
    public void testarSubtracao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(5);
        numeros.add(1);
        numeros.add(3);
        Assertions.assertEquals(Operacao.subtracao(numeros), 1);
    }

    @Test
    public void testarSubtracaoDoisNumeros(){
        Assertions.assertEquals(Operacao.subtracao(5,3), 2);
    }

    @Test
    public void testarMultiplicacao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(3);
        numeros.add(2);
        Assertions.assertEquals(Operacao.multiplicacao(numeros), 12);
    }

    @Test
    public void testarMultiplicacaoDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplicacao(2,3), 6);
    }

    @Test
    public void testarDivisao(){
        Assertions.assertEquals(Operacao.divisao(2,6), 3);
    }

    @Test
    public void testarValorZeradoDivisao(){
        Assertions.assertThrows(ArithmeticException.class, () -> {Operacao.divisao(0,1);});
    }

}
